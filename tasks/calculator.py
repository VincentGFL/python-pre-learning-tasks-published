def calculator(number1, number2, operator):
    # ==============
    number1 = int(input("Please enter first number: "))
    number2 = int(input("Please enter second number: "))
    operator = input("Please enter your operator: ")

    if operator == "+":
        print(number1 + number2)

    elif operator == "-":
        print(number1 - number2)

    elif operator == "*":
        print(number1 * number2)

    elif operator == "/":
        print(number1 / number2)
    else:
        print("Invalid input. Please try again")

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console

